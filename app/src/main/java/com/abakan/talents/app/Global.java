package com.abakan.talents.app;

import android.content.Context;

import com.abakan.talents.models.User;
import com.abakan.talents.tools.Retrofit.PrefData;


/**
 * Created by Abandah on 2/12/2018.
 */

public class Global {
    public static Long UserId;
    public static String AuthCode;
    public static String ClientToken;
    private static User user;


    public static String getClientToken() {
        return ClientToken;
    }

    public static void setClientToken(String clientToken) {
        ClientToken = clientToken;
    }

    public static String getAuthCode(Context context) {
        if (AuthCode == null || AuthCode.equalsIgnoreCase(""))
            GetPref(context);
        return AuthCode;
    }


    public static boolean GetPref(Context activity) {
        String ui = PrefData.getStringPrefs(activity, PrefData.UserId, "0");
        String lt = PrefData.getStringPrefs(activity, PrefData.AuthCode, "");
        if (ui.equals("") || lt.equalsIgnoreCase("")) {
            return false;
        }
        UserId = Long.parseLong(ui);
        AuthCode = lt;
        return true;
    }



    public static boolean SavePref(Context activity, String authcode, Long userId, int step) {
        PrefData.setStringPrefs(activity, PrefData.AuthCode, String.valueOf(authcode));
        PrefData.setStringPrefs(activity, PrefData.UserId, String.valueOf(userId));
        return GetauthPref(activity, activity);
    }

    private static boolean GetauthPref(Context activity, Context activity1) {
        String ac = PrefData.getStringPrefs(activity, PrefData.AuthCode, "0");
        String ui = PrefData.getStringPrefs(activity, PrefData.UserId, "0");
        AuthCode = ac;
        UserId = Long.valueOf(ui);
        return true;
    }


    public static void setUser(Context c, User userob) {
        if(userob == null )
            return;
        PrefData.setStringPrefs(c,PrefData.UserOBJ, Util.Gson().toJson(userob).toString());
        user = userob;
    }

    public static User getUser() {
        if (user == null )
        {
            getUser(App.getContext());
        }
        return user;
    }
    private static void getUser(Context c) {
       String js =PrefData.getStringPrefs(c,PrefData.UserOBJ,"");
       user = Util.Gson().fromJson(js,User.class);
    }
}

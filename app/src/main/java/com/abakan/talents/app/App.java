package com.abakan.talents.app;

import android.app.Application;
import android.content.Context;

import androidx.appcompat.app.AppCompatDelegate;


/**
 * Created by Abandah on 2/15/2018.
 */

public class App extends Application {


    private static Context mApp = null;

    @Override
    public void onCreate() {
        super.onCreate();
        mApp = this;
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
      /*  if (!BuildConfig.DEBUG)
            new UCEHandler.Builder(this)
                    .setTrackActivitiesEnabled(false)
                    .addCommaSeparatedEmailAddresses(getString(R.string.comma_separated_email_addresses))
                    .build();*/
    }

    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        //  MultiDex.install(this);
    }

    public static Context getContext() {
        return mApp.getApplicationContext();
    }
}

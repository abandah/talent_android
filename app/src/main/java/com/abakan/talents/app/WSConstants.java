package com.abakan.talents.app;

public class WSConstants {

    public static final String website="https://chat.meemwaw.com:7001";
    public static final String internettestLink="chat.meemwaw.com";
    // public static final String thumbanail =website+"/DCC/resizeimage.aspx?width=150&height=150&crop=true&image=";
    public static final String thumbanailprofile = website+"/DCC/resizeimage.aspx?width=80&height=80&crop=true&image=";
    public static final String thumbanailsimple = website+"/DCC/resizeimage.aspx?width=40&height=40&crop=true&image=";
    public static final String thumbanailmainprofile = website+"/DCC/resizeimage.aspx?width=200&height=200&crop=true&image=";
    public static final String thumbanailuplodedpic = website+"/DCC/resizeimage.aspx?width=500&height=400&crop=true&image=";
    public static final String API_URL = website+"/";
    public static final String siteurl = website;
    public static String UserImgePath = "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcT_ANU4t8VnR3VvSwOgzaVgW9C8R9ushLjQoi3ffrrt_ulmdKtn&usqp=CAU";
    public static String Link = "https://meet.jit.si";
    // public static String Link = "https://vctester.com";

}
//https://www.mimwa.com/chat/UploadFile
package com.abakan.talents.app;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.StrictMode;

import com.abakan.talents.tools.Retrofit.DateDeserializer;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.net.InetAddress;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class Util {

   /* public static void ShowWarning(Context context, String message) {
        Snackbar.with(context, null)
                .type(Type.WARNING)
                .message(message)
                .duration(Duration.LONG)
                .fillParent(true)
                .textAlign(Align.CENTER)
                .show();
    }

    public static void ShowERROR(Context context, String message) {
        Snackbar.with(context, null)
                .type(Type.ERROR)
                .message(message)
                .duration(Duration.LONG)
                .fillParent(true)
                .textAlign(Align.CENTER)
                .show();
    }*/

    public static final String DATE_FORMAT_2 = "dd-MMM-yyyy";
    public static final String DATE_FORMAT_1 = "hh:mm";

    public static String getTimeFromDate(Date today) {
        if (today == null)
            return "00:00";
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT_1);
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return dateFormat.format(today);
    }

    public static Gson Gson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
       /* gsonBuilder.setExclusionStrategies(new ExclusionStrategy() {
            @Override
            public boolean shouldSkipField(FieldAttributes f) {
                return f.getDeclaringClass().equals(RealmObject.class);
            }

            @Override
            public boolean shouldSkipClass(Class<?> clazz) {
                return false;
            }
        });*/
        gsonBuilder.registerTypeAdapter(Date.class, new DateDeserializer());
        Gson gson = gsonBuilder.create();
        return gson;
    }

    public static String getAppKey() {
        Context activity = App.getContext();
        ApplicationInfo applicationInfo = null;
        try {
            applicationInfo = activity.getPackageManager().getApplicationInfo(activity.getPackageName(), PackageManager.GET_META_DATA);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return applicationInfo.metaData.getString("com.google.android.geo.API_KEY");
    }


    public static boolean isInternetAvailable() {
        try {
            int SDK_INT = android.os.Build.VERSION.SDK_INT;
            if (SDK_INT > 8) {
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                        .permitAll().build();
                StrictMode.setThreadPolicy(policy);
            }
            InetAddress ipAddr = InetAddress.getByName(WSConstants.internettestLink);
            //You can replace it with your name
            return !ipAddr.equals("");

        } catch (Exception e) {
            return false;
        }
    }
}

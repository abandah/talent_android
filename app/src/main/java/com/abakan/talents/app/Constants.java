package com.abakan.talents.app;

/**
 * Created by Abandah on 3/26/2018.
 */

public class Constants {




    public static String text_plain ="text/plain";
    public static CharSequence spaceASKI="spaceASKI";

    public static final int TextType = 1;
    public static final int ImageType = 2;
    public static final int LocationType = 5;
    public static final int WebViewType = 8;
    public static final int BarcodeType = 10;
    public static final int NFCType = 11;

    public Constants() {
    }

    public static final String GOOGLE_serverClientId="843133879273-8t5qdj90d7966cuagutsoqoqm0gshoji.apps.googleusercontent.com";
    //public static final String FACEBOOK_AppId="381503995656131";
    public static final String FACEBOOK_AppId="1155545027925828";

   /* <string CategoryName="facebook_app_id">381503995656131</string>
<string CategoryName="fb_login_protocol_scheme">fb381503995656131</string>*/

    //info@lenvosoft.com
    public static final String TWITTER_ApiKey="B6Mj2ace0iHJssAdyEGhrGPRI";
    public static final String TWITTER_SecreteKey="FBN6pkPpFG7S1SnMgzTMAgjsYNhmuYmHvnu4buKyUZ6NoKYdT2";

    //info@lenvosoft.com
    //www.sinch.com

    //public static final String DateFormat ="yyyy/MM/dd" ;
    public static final String DateFormat ="dd/MM/yyyy" ;
    public static final String TimeFormat ="HH:mm:ss" ;
    public static final String DateTimeFormat =DateFormat+" "+TimeFormat;
    public static float alfaOnDone=0.4f;


    public static int view =1;
    public static int noOpration =0;
    public static int like =2;
    public static int superlike =3;
    public static int dislike =4;

    //Progress dialog time period
    public static int ShortProgressBarDuration =500;
    public static int SuperLongProgressBarDuration =1500;
    public static int SuperUltraLongProgressBarDuration =3000;
    public static int ItemListProgressBarDuration =100;
    public static int LongProgressBarDuration =1000;




    public static int Crop_x =1;
    public static int Crop_y =1;

    public static int Crop_x_submit =270;
    public static int Crop_y_submit =209;


    public static int BackgrawndAlpha=40;

    public final static String AuthCodeHeaderName="__AuthCode";
    public final static String ClientTokenHeaderName="__ClientToken";
    public final static String AccessTokenHeaderName="__AccessToken";
    public static int ListMaxItemNumber =10;
    public static int ListFirstItemIndex = 0;

}

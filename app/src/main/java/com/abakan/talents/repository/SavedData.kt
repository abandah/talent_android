package com.abakan.talents.repository

import com.abakan.talents.app.App
import com.abakan.talents.app.Global
import com.abakan.talents.tools.Retrofit.PrefData

class SavedData {
    companion object {
        var userId: String
            get() = PrefData.getStringPrefs(App.getContext(), PrefData.UserId, null)
            set(value: String) {
                PrefData.setStringPrefs(App.getContext(), PrefData.UserId, value)
            }
        var authCode: String
            get() = PrefData.getStringPrefs(App.getContext(), PrefData.AuthCode, null)
            set(value: String) {
                PrefData.setStringPrefs(App.getContext(), PrefData.AuthCode, value)
            }
        var isVarifed: Int
            get() = PrefData.getIntPrefs(App.getContext(), PrefData.IsVarifed, 0)
            set(value: Int) {
                PrefData.setIntPrefs(App.getContext(), PrefData.IsVarifed, value)
            }
        var clientToken: String
            get() = Global.ClientToken
            set(value: String) {
                Global.ClientToken = value
            }
    }
}
package com.app.tiktok.base

import android.icu.text.CaseMap
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.abakan.talents.R
import com.abakan.talents.tools.Retrofit.PrefData


open class BaseActivity(val activityMain: Int) : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(activityMain)

    }


    public fun getColorFromRes(barColor: Int): Int {
        return ContextCompat.getColor(this, barColor)
    }

    fun showTitle(show : Boolean){
        if(show){
            supportActionBar?.show()
            return
        }
        supportActionBar?.hide()
    }

    fun setTitle(title : String){
        supportActionBar?.setTitle(title)
    }




}
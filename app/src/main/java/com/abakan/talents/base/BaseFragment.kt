package com.app.tiktok.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.abakan.talents.base.BaseViewModel

abstract class BaseFragment(
    contentLayoutId: Int,
    val showTitle: Boolean,
    private var viewModel: BaseViewModel
) :
    Fragment(contentLayoutId) {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        showTitle(showTitle);
        (activity as BaseActivity).setTitle(getStringFromContext(setTitle()))
        return super.onCreateView(inflater, container, savedInstanceState)


    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

      //  viewModel = ViewModelProvider(this).get(BaseViewModel::class.java)
        viewModel = ViewModelProvider(this).get(viewModel.javaClass)
        viewModel.setcontext(this)
        viewModel.onViewCreated(view,savedInstanceState)

    }

    fun getViewModel() :BaseViewModel{
        return viewModel
    }
    fun showTitle(show: Boolean) {
        (activity as BaseActivity).showTitle(show)
        // supportActionBar.hide()
    }
    abstract fun setTitle() : Int;

    fun getStringFromContext(id : Int) : String{
        return  resources.getString(id)
    }

    override fun onStart() {
        super.onStart()
        viewModel.onStart()
    }

    override fun onPause() {
        super.onPause()
        viewModel.onPause()
    }

    override fun onStop() {
        super.onStop()
        viewModel.onStop()
    }

    override fun onResume() {
        super.onResume()
        viewModel.onResume()
    }
}
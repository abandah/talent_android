package com.abakan.talents.base

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModel
import com.app.tiktok.base.BaseFragment

abstract class BaseViewModel : ViewModel() {


    abstract fun print() ;
    abstract fun onStart()
    abstract fun onPause()
    abstract fun  onStop()
    abstract fun  onResume()
    abstract fun onViewCreated(view: View, savedInstanceState: Bundle?)
    abstract fun setcontext(baseFragment: BaseFragment)
}
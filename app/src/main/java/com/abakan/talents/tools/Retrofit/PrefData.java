package com.abakan.talents.tools.Retrofit;

import android.content.Context;

/**
 * Created by c161 on 14/03/16.
 */
public class PrefData {


    public static final String PREFERENCE = "SHOPPINGLIST";

    //==================Abandah==========================


    //====================================================

    public static String UserId = "UserId";
    public static String AuthCode="";
    public static String IsVarifed="IsVarifed";
    public static String UserOBJ="UserOBJ";


    public static boolean setStringPrefs(Context context, String prefKey, String Value) {
        return context.getSharedPreferences(PREFERENCE, Context.MODE_PRIVATE).edit().putString(prefKey, Value).commit();
    }

    public static String getStringPrefs(Context context, String prefKey, String defaultValue) {
        return context.getSharedPreferences(PREFERENCE, Context.MODE_PRIVATE).getString(prefKey, defaultValue);
    }

    public static boolean setIntPrefs(Context context, String prefKey, int value) {
        return context.getSharedPreferences(PREFERENCE, 0).edit().putInt(prefKey, value).commit();
    }

    public static int getIntPrefs(Context context, String prefKey, int defaultValue) {
        return context.getSharedPreferences(PREFERENCE, Context.MODE_PRIVATE).getInt(prefKey, defaultValue);
    }

    public static boolean clearWholePreference(Context context) {
        return context.getSharedPreferences(PREFERENCE, Context.MODE_PRIVATE).edit().clear().commit();
    }

    /**
     * Clear single key value
     *
     * @param prefKey
     * @param context
     */
    public static boolean remove(Context context, String prefKey) {
        return context.getSharedPreferences(PREFERENCE, Context.MODE_PRIVATE).edit().remove(prefKey).commit();
    }



}
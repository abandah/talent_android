package com.abakan.talents.tools;

public interface PickedContactListener {
    void onContactPicked(PickedContact pickedContact);
}

package com.abakan.talents.tools.Retrofit;

import android.content.Context;
import android.content.DialogInterface;

import androidx.appcompat.app.AlertDialog;

import com.abakan.talents.R;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Abandah on 6/25/2019.
 */
public class CallbackHandler implements Callback<WSResponse> {

    private static final int TOTAL_RETRIES = 3;
    private static final String TAG = CallbackHandler.class.getSimpleName();
    private final Call<WSResponse> call;
    private final Context context;
    private boolean showloader = false;
    private boolean showdialog = true;
    private int retryCount = 0;
    private int counter = 0;
    AlertDialog m = null;
    public CallbackHandler(Call<WSResponse> call, Context context, boolean showloader, boolean showdialog) {
        this.call = call;
        this.context = context;
        this.showloader = false;
        this.showdialog = showdialog;
        if (this.showloader) {

        }
    }

    @Override
    public void onResponse(Call<WSResponse> call, Response<WSResponse> response) {
        WSResponse response1 = response.body();
        if (showloader) {
        }

        if (response1 == null) {

            if (showdialog) {

               // ShowRetry(response.message());
            }
            onResponseFailure(new Throwable("Null response"));
            return;
        }

        onResponseSuccessful(response1);

    }

    @Override
    public void onFailure(Call<WSResponse> call, Throwable t) {
        if (showdialog) {
           // ShowRetry(t.getMessage());
        }
        if (showloader) {
            //hide loader
        }

        onResponseFailure(t);

    }

    protected void onResponseSuccessful(WSResponse wsResponse) {
    }

    protected void onResponseFailure(Throwable response_is_null) {
    }

    private void ShowRetry(String a) {
        if(m != null && m.isShowing()){
            return;
        }
        a = context.getString(R.string.you_have_an_error_with_your_connection);
        if (counter < 3) {
            counter++;
            retry();
            return;
        }
        counter = 0;
         m = new MaterialAlertDialogBuilder(context)
                //.setTitle(getString(R.string.caution))
                .setMessage(a)
                .setPositiveButton(context.getString(R.string.retry), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        retry();
                        dialog.dismiss();
                        m = null;
                        counter = 0;
                    }
                })
                .setNegativeButton(context.getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                        m = null;
                        counter = 0;
                    }
                })
                .show();
        m = null;

    }

    private void retry() {
        if (showloader) {
        }

        call.clone().enqueue(this);

    }

    private void ShowRetry() {
        ShowRetry(context.getString(R.string.you_have_an_error_with_your_connection));
//        General_Dialog general_dialog = new General_Dialog(context);
//        general_dialog.setMessage(context.getString(R.string.you_have_an_error_with_your_connection));
//        general_dialog.setbuttons(context.getString(R.string.cancel), null, context.getString(R.string.retry));
//        general_dialog.setOnClicklistener(new General_Dialog.GeneralDialogListener() {
//            @Override
//            public void onRightButtonClick(View s) {
//            retry();
//            }
//
//            @Override
//            public void onLeftButtonClick() {
//
//            }
//
//            @Override
//            public void onMidelButtonClick() {
//
//            }
//        });
//        general_dialog.show();

    }

}
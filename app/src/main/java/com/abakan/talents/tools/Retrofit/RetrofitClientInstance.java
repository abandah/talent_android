package com.abakan.talents.tools.Retrofit;

import android.content.Context;

import com.abakan.talents.app.Constants;
import com.abakan.talents.app.Global;
import com.abakan.talents.app.WSConstants;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.Collections;
import java.util.Date;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Protocol;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClientInstance {

    private static final String BASE_URL = WSConstants.API_URL;

    public static Retrofit getRetrofitInstance(String AccessToken, Context context) {
        OkHttpClient.Builder client = new OkHttpClient.Builder();
        client.addInterceptor(new Interceptor() {

            @Override
            public Response intercept(Chain chain) throws IOException {
                Request newRequest = chain.request().newBuilder()
                        .addHeader(Constants.AccessTokenHeaderName,
                                AccessToken == null ? "" : AccessToken)
                        .addHeader(Constants.ClientTokenHeaderName,
                                Global.getClientToken() == null ? "" : Global.getClientToken())
                        .addHeader(Constants.AuthCodeHeaderName,
                                Global.getAuthCode(context) == null ? "" : Global.getAuthCode(context))
                        .build();
                return chain.proceed(newRequest);
            }
        });//.protocols(Arrays.asList(Protocol.HTTP_2, Protocol.HTTP_1_1));
        client.protocols(Collections.singletonList(Protocol.HTTP_1_1));
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Date.class, new DateDeserializer());
        Gson gson = gsonBuilder.setLenient().create();
        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client.build())
                .addConverterFactory(GsonConverterFactory.create(gson)).build();

    }

}


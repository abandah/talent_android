package com.abakan.talents.tools;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

public class ContactPicker extends Fragment {

    private static String TAG = "ContactPicker";
    private int requestCode = 23;
    private PickedContactListener onContactPicked;

    public static ContactPicker create(AppCompatActivity activity, PickedContactListener onContactPicked){

        ContactPicker picker = new ContactPicker();
            picker.onContactPicked = onContactPicked;
            activity.getSupportFragmentManager().beginTransaction()
                    .add(picker, TAG)
                    .commitNowAllowingStateLoss();

            return  picker;

            //onFailure(e)

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return  null;
    }

    public void pick(){
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setData(ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
        startActivityForResult(intent, requestCode);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if(requestCode == this.requestCode ){
                Cursor  cursor = null;
                try {
                    Uri uri = data.getData();
                     cursor =  getActivity().getContentResolver().query(uri, null, null, null, null);
                    if(cursor.moveToFirst()){
                        String phone = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        String name = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                        cursor.close();
                        onContactPicked.onContactPicked(new PickedContact(phone, name));
                    }


                } catch ( Exception e) {
                   // onFailure(e)
                    cursor.close();
                }
            }

        }
    }


}


package com.abakan.talents.tools.Retrofit;


import com.abakan.talents.app.Constants;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface Offer_Client {

    @FormUrlEncoded
    @POST("API/MobileAppWebAPI/GetAuthCode")
    Call<WSResponse> GetAuthCode(@Field("UserId") Long UserId);

    // @Headers(Constants.AuthCodeHeaderName+":"+ Global.getAuthCode())
    @FormUrlEncoded
    @POST("API/MobileAppWebAPI/GetClientToken")
    Call<WSResponse> GetClientToken(@Header(Constants.AuthCodeHeaderName) String AuthCode,
                                    @Field("UserId") Long UserId,
                                    @Field("Lang") String Lang);

    @FormUrlEncoded
    @POST("API/MobileAppWebAPI/GetAccessToken")
    Call<WSResponse> getAccessToken(@Field("UserId") Long UserId, @Header(Constants.AuthCodeHeaderName) String AuthCode,
                                    @Header(Constants.ClientTokenHeaderName) String ClientToken);
    @FormUrlEncoded
    @POST("API/MobileAppWebAPI/Post")
    Call<WSResponse> Call_API(@Field("Operation") String operation,
                              @Field("Model") String model,
                              @Field("InputObject") String inputObject,
                              @Field("IdentityObject") String identityObject);



    @Multipart
    @POST("en/Chat/UploadFileJSON")
    Call<WSResponse> UploadFile(@Part MultipartBody.Part file,
                                 @Part("UserId") RequestBody UserId,
                                 @Part("UploadType") RequestBody UploadType );
}


package com.abakan.talents.tools.Retrofit;


import com.abakan.talents.app.Util;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

public class WSResponse {

    @SerializedName("Status")
    private int Status;
    @SerializedName("StatusMessage")
    private String StatusMessage;

    @SerializedName("Value")
    private JsonElement Value;

    @Override
    public String toString() {
        return "WSResponse{" +
                "Status=" + Status +
                ", StatusMessage='" + StatusMessage + '\'' +
                ", Value=" + Value +
                '}';
    }

    public int getStatus() {
        return Status;
    }

    public String getStatusMessage() {
        return StatusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        StatusMessage = statusMessage;
    }

    public JsonArray getTable(String key) {
        return Value.getAsJsonObject().get(key).getAsJsonArray();
    }


    public <CLS> ArrayList<CLS> getData(String table, CLS cls) {
       // Arrays.asList(new GsonBuilder().create().fromJson("sdfsdf", cls));
       // Type uy = new TypeToken<ArrayList<cls>>() {}.getType();
        ArrayList<CLS> sada = Util.Gson().fromJson(getTable("Table"), new TypeToken<ArrayList<?>>() {
        }.getType());

        return  sada;
    }
}

package com.abakan.talents.tools.Retrofit;

import android.content.Context;

import com.abakan.talents.app.Global;
import com.abakan.talents.app.Util;
import com.abakan.talents.app.WSConstants;
import com.google.gson.JsonObject;


import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Retrofit;

public class Call {

    Listener listener;
    String Operation = "";
    String Model = "";
    String InputObject = "{}";
    String IdentityObject = "{}";
    private File file;

    public static Call CustomSP(String Model) {
        Call c = new Call();
        c.Operation = "CustomSP";
        c.Model = Model;
        return c;
    }

    public static Call Operation(String Operation, String Model) {
        Call c = new Call();
        c.Operation = Operation;
        c.Model = Model;
        return c;
    }

    public static Call UploadFile(File file) {
        Call c = new Call();
        c.file = file;
        return c;
    }

    public Call setInputObject(JsonObject inputObject) {
        this.InputObject = Util.Gson().toJson(inputObject);
        return this;

    }

    public Call setIdentityObject(JsonObject identityObject) {
        this.IdentityObject = Util.Gson().toJson(identityObject);
        return this;
    }

    public Call setInputObjectAsString(String inputObject) {
        this.InputObject = inputObject;
        return this;

    }

    public Call setIdentityObjectAsString(String identityObject) {
        this.IdentityObject = identityObject;
        return this;
    }

    public Call WithListener(Listener listener) {
        this.listener = listener;
        return this;

    }

    public void Run(Context context, boolean WithAuth, String WithHeader) {

        if (!WithAuth) {
            Call_API(WithHeader, context);
        } else {
            Call_API_WithAuth(context);
        }
    }

    public void Run(Context context, boolean WithAuth) {
        if (Util.isInternetAvailable()){
            Run(context, WithAuth, "");
        }else{
            listener.offlineHandler();
        }
    }

    public void Run(Context context) throws Exception {
        if (file != null) {
            Call_API_Upload(context);
        } else {
            throw new Exception("Wrong method");
        }
    }

    private void Call_API(String AccessToken, Context context) {

        Retrofit retrofit = RetrofitClientInstance.getRetrofitInstance(AccessToken, context);
        Offer_Client offer_client = retrofit.create(Offer_Client.class);
        retrofit2.Call call = offer_client.Call_API(Operation, Model, InputObject, IdentityObject);
        call.enqueue(new CallbackHandler(call, context, false, true) {
            @Override
             public void onResponseSuccessful(WSResponse wsResponse) { super.onResponseSuccessful(wsResponse);
                listener.onResponseSuccessful(wsResponse);
            }

            @Override
            public void onResponseFailure(Throwable response_is_null) {
                listener.onResponseFailure(response_is_null);
            }
        });
    }

    @Override
    public String toString() {
        return "Call{" +
                "Operation='" + Operation + '\'' +
                ", Model='" + Model + '\'' +
                ", InputObject='" + InputObject + '\'' +
                ", IdentityObject='" + IdentityObject + '\'' +
                '}';
    }

    private void Call_API_Upload(Context context) {
        MultipartBody.Part part = null;
        RequestBody requestImageFile = RequestBody.create(MediaType.parse("file"), file);
        part = MultipartBody.Part.createFormData("any_name_for_the_part", file.getName(), requestImageFile);
        RequestBody UserId = RequestBody.create(MediaType.parse("text/plain"), Global.getUser().UserId);
        RequestBody UploadType = RequestBody.create(MediaType.parse("text/plain"), "2");

        Retrofit retrofit = RetrofitClientInstance.getRetrofitInstance("", context);
        Offer_Client offer_client = retrofit.create(Offer_Client.class);
        retrofit2.Call<WSResponse> call = offer_client.UploadFile(part, UserId, UploadType);
        call.enqueue(new CallbackHandler(call, context, false, true) {
            @Override
             public void onResponseSuccessful(WSResponse wsResponse) { super.onResponseSuccessful(wsResponse);
                String Link = wsResponse.getTable("Table").get(0).getAsJsonObject().get("FilePath").getAsString();
                if (Link == null) {
                    listener.onResponseFailure(new Throwable());
                    return;
                }
                if (Link.length() < 6) {
                    listener.onResponseFailure(new Throwable());
                    return;
                }
                if (Link.startsWith("~")) {
                    Link = Link.replace("~", WSConstants.siteurl);
                }
                wsResponse.setStatusMessage(Link);
                listener.onResponseSuccessful(wsResponse);
            }

            @Override
            public void onResponseFailure(Throwable response_is_null) {
                listener.onResponseFailure(response_is_null);
            }
        });
    }

    private void Call_API_WithAuth(Context context) {
        Retrofit retrofit = RetrofitClientInstance.getRetrofitInstance("", context);
        Offer_Client offer_client = retrofit.create(Offer_Client.class);
        retrofit2.Call<WSResponse> call = offer_client.Call_API("__AFG_AccessToken", "Users", "{}", "{}");
        call.enqueue(new CallbackHandler(call, context, false, true) {
            @Override
             public void onResponseSuccessful(WSResponse wsResponse) { super.onResponseSuccessful(wsResponse);
                try {
                    String authCode = wsResponse.getTable("OutPut").get(0).getAsJsonObject().get("__AccessToken").getAsString();
                    Call c = Call.Operation(Operation, Model)
                            .setInputObjectAsString(InputObject)
                            .setIdentityObjectAsString(IdentityObject)
                            .WithListener(new Listener() {
                                @Override
                                 public void onResponseSuccessful(WSResponse wsResponse) { super.onResponseSuccessful(wsResponse);
                                    listener.onResponseSuccessful(wsResponse);
                                }

                                @Override
                                public void onResponseFailure(Throwable response_is_null) {
                                    listener.onResponseFailure(response_is_null);
                                }


                            });
                    c.Run(context, false, authCode);
                } catch (Exception e) {
                    listener.onResponseFailure(e);
                }
            }

            @Override
            public void onResponseFailure(Throwable response_is_null) {
                listener.onResponseFailure(response_is_null);
            }
        });

    }
}

package com.abakan.talents.tools.Retrofit;


abstract public class Listener {
    protected void onResponseSuccessful(final WSResponse wsResponse) {
    }

    protected  void onResponseFailure(Throwable response_is_null) {
    }

    protected  void offlineHandler() {
    }
}

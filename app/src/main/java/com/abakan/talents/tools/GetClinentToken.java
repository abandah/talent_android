package com.abakan.talents.tools;


import com.abakan.talents.app.App;
import com.abakan.talents.app.Global;
import com.abakan.talents.tools.Retrofit.Call;
import com.abakan.talents.tools.Retrofit.Listener;
import com.abakan.talents.tools.Retrofit.PrefData;
import com.abakan.talents.tools.Retrofit.WSResponse;
import com.google.gson.JsonObject;


public class GetClinentToken {
    static BackOnlineHandler listener;

    public static void GetClientToken(BackOnlineHandler backOnlineHandler) {
        listener = backOnlineHandler;
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("UserId", PrefData.getStringPrefs(App.getContext(), PrefData.UserId, ""));
        Call c = Call.Operation("__AFG_ClientToken", "Users")
                .setIdentityObject(jsonObject)
                .WithListener(new Listener() {
                    @Override
                     public void onResponseSuccessful(WSResponse wsResponse) { super.onResponseSuccessful(wsResponse);
                        if (wsResponse.getStatusMessage().equalsIgnoreCase("Incorrect AuthCode")) {
                            //StartRegister();
                            return;
                        }
                        try {
                            String authCode = wsResponse.getTable("OutPut").get(0).getAsJsonObject().get("__ClientToken").getAsString();
                            Global.ClientToken = authCode;
                            listener.appBackOnline();
                            //Users_select_ById();

                        } catch (Exception e) {
                            //GetClientToken(id);
                        }


                    }

                    @Override
                    public void onResponseFailure(Throwable response_is_null) {
                        // GetClientToken(id);
                    }

                    @Override
                    protected void offlineHandler() {
                        Global.ClientToken = null;
                        //Users_select_ById();
                    }
                });
        c.Run(App.getContext(), false);
    }

}

package com.abakan.talents.viewmodel

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import com.abakan.talents.base.BaseViewModel
import com.abakan.talents.fragment.SplashFragment
import com.abakan.talents.repository.SavedData
import com.abakan.talents.tools.Retrofit.PrefData
import com.app.tiktok.base.BaseFragment

class SplashViewModel() : BaseViewModel() {
    lateinit var context: SplashFragment
    override fun print() {
    }


    override fun onStart() {


    }

    override fun onPause() {
    }

    override fun onStop() {
    }

    override fun onResume() {
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        Handler(Looper.getMainLooper()).postDelayed({
            start()
        }, 1000)
    }

    override fun setcontext(baseFragment: BaseFragment) {
        this.context = baseFragment as SplashFragment
    }

    private fun start() {
        if (SavedData.userId.isNullOrEmpty() && SavedData.authCode.isNullOrEmpty()) {
            if (SavedData.isVarifed == 0) {
             //   context.StartRegister()
                context.StartApp()
            } else {
                context.StartApp()
            }
        } else {
            context.StartApp()
        }
    }


}
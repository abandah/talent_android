package com.abakan.talents.models;


import com.abakan.talents.app.WSConstants;

public class User  {

    public String UserId;
    public String UserDisplayName;
    private String UserImageFilePath;
    public String PhoneNumber;
    public String Email;

    public User() {
    }


    public String getUserImageFilePath() {
        if (UserImageFilePath == null)
            return WSConstants.UserImgePath;
        if (UserImageFilePath.length() < 6)
            return WSConstants.UserImgePath;
        if (UserImageFilePath.startsWith("~"))
            UserImageFilePath.replace("~", WSConstants.siteurl);

        return WSConstants.thumbanailprofile + UserImageFilePath;
    }

    public String getUsermainImageFilePath() {
        if (UserImageFilePath == null)
            return WSConstants.UserImgePath;
        if (UserImageFilePath.length() < 6)
            return WSConstants.UserImgePath;
        if (UserImageFilePath.startsWith("~"))
            UserImageFilePath.replace("~", WSConstants.siteurl);

        return WSConstants.thumbanailmainprofile + UserImageFilePath;
    }

    public void setUserImageFilePath(String userImageFilePath) {
        UserImageFilePath = userImageFilePath;
    }

    public String getEmail() {
        return Email == null ? "" : Email;
    }


}

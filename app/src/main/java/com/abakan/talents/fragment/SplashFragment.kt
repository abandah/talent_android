package com.abakan.talents.fragment

import android.app.NotificationManager
import android.content.Context
import com.abakan.talents.R
import com.abakan.talents.viewmodel.SplashViewModel
import com.app.tiktok.base.BaseFragment

class SplashFragment : BaseFragment(R.layout.fragment_splash, false, SplashViewModel()) {


    override fun setTitle(): Int {
        return R.string.Splash
    }

    override fun onStart() {
        try {
            val notificationManager =
                activity?.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.cancelAll()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        super.onStart()
        //getViewModel().print()

    }

    fun StartRegister() {

    }

    fun StartApp() {


    }
}